import shutil
from os.path import dirname, join

from packagetools.builder import *
from packagetools.command import run


class Recipe(PackageRecipe):
    def describe(self) -> PackageInfo:
        return PackageInfo(
            description="just an hello world example",
            id="hello-world",
            pretty_name="Hello World!",
            version="0.1",
        )

    def checkout(self):
        src_dir = join(dirname(__file__), "src")
        shutil.copytree(src_dir, "src")
        os.chdir("src")

    def build(self):
        run("cl.exe", "src.cc", "/c", "/EHsc")
        run("link.exe", "src.obj", "/out:bin.exe")

    def test(self):
        run("bin.exe")

    def package(self, out_dir: str):
        shutil.move("bin.exe", out_dir)


PackageBuilder(Recipe()).make()
